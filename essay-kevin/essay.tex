% Dokumenteinstellungen
% ======================================================================	

\documentclass[10pt,a4paper,parskip=half,DIV=10]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{scientific}
\usepackage[european]{circuitikz}
\usepackage[automark]{scrpage2}
\usepackage{titling}
\usepackage{bbm}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage[square,numbers]{natbib}

\pagestyle{scrheadings}
\clearscrheadfoot
\ihead[]{\thetitle}
\ohead[]{\theauthor}
\cfoot[]{\pagemark}

\sisetup{locale=DE}

\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\rang}{rang}

% Dokumentbeginn
% ======================================================================

\begin{document}
	
	\title{Vergleich SIFT - SURF}
	\subtitle{Essay Computer Vision}
	\date{\today}
	\author{Kevin Meyer}
	\maketitle
	
%	\nocite{*}
	
	\section{Abstract}
	Dieses Essay fasst zwei gängige Methoden zur Merkmalsbestimmung zusammen und vergleicht diese miteinander. Die Merkmale sollen vergleichbar sein und sie stellen dabei die zur Wiedererkennung entscheidenden Punkte von Bildern bzw. Objekten dar. Anhand dieser Features können Bildinhalte verglichen werden (z.B. Objekterkennung) auch, wenn die Bilder unter verschiedenen Rahmenbedingungen erstellt wurden.
	
	Zwei Ansätze zur Merkmalsextraktion wurden von Lowe (SIFT, \cite{Sift}) und Bay (SURF, \cite{Surf}) entwickelt. Beide Verfahren sind populär und oft eingesetzt bzw. implementiert. Diese Arbeit kommt zu dem Ergebnis, dass SURF bei gleicher Qualität effizienter arbeitet, da es als Weiterentwicklung von SIFT gesehen werden kann.
	
	\section{Einleitung}
	Die Merkmalserkennung ist eines der wichtigsten Themen in der Bildverarbeitung und der Computer Vision. Diese markanten Punkte sind die entscheidenden zur (Wieder-)Erkennung von Objekten oder Szenen aus einem Bild. Mithilfe dieser Punkte bewerten sowohl Mensch wie auch der Computer das optisch Wahrgenommene.
	
	Durch Algorithmen soll diese Merkmalserkennung unabhängig von Kamera- und Umwelteinflüssen, wie Rotation, Position, Skalierung, Perspektive, Helligkeit, Kontrast oder Bildrauschen, machen. Die zu vergleichenden Methoden sind SIFT \cite{Sift} und SURF \cite{Surf}, die auf dem Gebiet der Computer Vision zu den Standardverfahren gehören.
	
	Dieses Essay stellt im Folgenden zunächst die generelle Arbeitsweise der Merkmalserkennung vor und geht im Anschluss auf ihre Unterschiede ein. Es fällt schnell auf, dass sich beide Methoden in die selbe Schrittreihenfolge gliedern lässt und diese sich größtenteils ähneln. Dieser Fakt ist auf die Tatsache zurück zu führen, dass SURF eine Weiterentwicklung bzw. Verbesserung von SIFT darstellt, welcher mehr als drei Jahre später veröffentlicht wurde.
	
	Diese Arbeit kommt zu dem Ergebnis, dass SURF, vor allem durch die veränderte Vorgehensweise bei der Detektierung und bei der Deskription der Merkmalspunkte, sich gegenüber SIFT in Effizienz und Robustheit verbessert. Dies gelingt SURF ohne an Qualität oder Funktionsumfang einzubüßen.
	
	\section{Genereller Ablauf}
	\label{sec:sift}
	Die in beiden Konzepten beschriebenen Verfahren lassen sich grob in folgende vier Schritte zusammenfassen:
	\begin{itemize}
		\item Detektierung der Extremwerte
		\item Lokalisierung der Merkmalspunkte
		\item Berechnung der Orientierung
		\item Deskription der gefundenen Punkte
	\end{itemize}
	
	Potentielle Merkmale werden mithilfe der Extremwerte lokalisiert. Diese werden im Anschluss auf Relevanz und Quantität gefiltert und unabhängig von ihrer Orientierung gemacht. Im letzten Schritt werden die gefundenen Merkmale beschrieben. Durch diesen finalen Arbeitsschritt werden Merkmalspunkte nun untereinander vergleichbar. Diese Vergleichbarkeit wird von der Objekterkennung gefordert. So können gefundene Merkmale entweder mit einer Merkmals-Datenbank oder mit den Ergebnissen aus Trainingsbildern verglichen werden, um gemeinsame Objekte zu erkennen und zu lokalisieren.
	
	Im Folgenden werden Detektor und Deskriptor beider Verfahren gegenüber gestellt.
	
	\section{Detektor}
	\subsection{SIFT}
	Der SIFT-Detektor arbeitet mithilfe der Difference of Gaussians (DoG) Methode. Die DoG Funktion $D(x,y,\sigma)$ ist, wie der Name andeutet, eine Differenz zweier Gaußfaltungen mit dem Bild. Hierbei wird die Faltung von Gaußfunktion und Bild Skalenraum $L(x,y,\sigma)$ genannt, womit sich ergibt:
	\begin{align*}
		L(x,y,\sigma) = G(x,y,\sigma)*I(x,y) \label{eq:L}
	\end{align*}
	mit $(x,y)$ als Merkmalspunkt, $I(x,y)$ als Ausgangsbild und 
	\begin{align*}
		G(x,y,\sigma) = \frac{1}{2\pi\sigma^2}\;e^{-\frac{(x^2+y^2)}{2\sigma^2}}
	\end{align*}
	
	Die Difference of Gaussians Funktion lässt sich nun schreiben als:
	\begin{align*}
		D(x,y,\sigma) &= (G(x,y,k\sigma) - G(x,y,\sigma))*I(x,y)\\
		&= L(x,y,k\sigma) - L(x,y,\sigma)
	\end{align*}
	
	Diese Matrizen werden nun für die verschiedenen Skalierungen berechnet und ergeben eine imaginäre Pyramidenstruktur (siehe \cite[Figure 1]{Sift}). Die eigentliche Extremwertbestimmung erfolgt nun durch Betrachtung der jeweiligen nächsten Nachbarn. Ein Maximum muss größer sein als seine insgesamt 26 Nachbarn (8 auf selber Ebene, jeweils 9 auf der nächst höheren bzw. tieferen, siehe \cite[Figure 2]{Sift}), ein Minimum entsprechend kleiner.

	\subsection{SURF}
	SURF verwendet eine einfache Approximation der Determinanten der Hesse-Matrix zur Detektion der Merkmale \citep{Surf}. In Verbindung mit der Verwendung von Integralbildern, welche eine schnelle Berechnung von Pixelsummen aus Rechtecken ermöglicht, spart dieser Ansatz Aufwand. Ein Integralbild speichert die Summe der Pixel, die vom Ursprung und dem aktuellen Punkt aufgespannt wird:
	\begin{align}
		I_\Sigma(x,y) = \sum_{i=0}^{i<x}\sum_{j=0}^{j<y}I(x,y)
	\end{align}
	Die von SURF verwendete Approximation der Determinante der Hessematrix lässt sich als Summe der Pixel in einem Rechteck beschreiben, die nach der einmaligen Erstellung des Integralbildes in konstanter Zeit berechnet wird:
	\begin{align}
		I_\Sigma = I_\Sigma(x,y) + I_\Sigma(x+\Delta x,y+\Delta y) - (I_\Sigma(x+\Delta x,y) + I_\Sigma(x,y+\Delta y))
		\label{eq:ISigma}
	\end{align}
	
	Die Hesse-Matrix $H$:	
	\begin{align}
		H(x, y, \sigma) = \begin{bmatrix} L_{xx}(x, y, \sigma) & L_{xy}(x, y, \sigma) \\  L_{yx}(x, y, \sigma) &  L_{yy}(x, y, \sigma)\end{bmatrix}
	\end{align}
	mit $L_{xx}(x, y, \sigma) = \frac{\partial^2}{\partial x^2}\, L(x,y,\sigma)$ (analog für $L_{xy}$ und $L_{yy}$). 
	
	\section{Deskriptor}
	\subsection{SIFT}
	Für die Deskription der gefundenen Merkmalspunkte betrachtet SIFT die Gradienten der Pixel um das Feature herum. Diese werden durch eine Gaußmatrix gewichtet. Anschließend wird das Quadrat der betrachteten Pixel in Abschnitte aufgeteilt. Für jeden dieser Abschnitte wird ein aus acht Elementen bestehender Vektor berechnet, der die Gradienten des Abschnittes repräsentiert.
	
	Die resultierenden Deskriptionen bestehen in der Praxis meist aus 128 Elementen (16 Vektoren/Abschnitte, 8 Elemente). Diese können dann untereinander mithilfe der nächsten Nachbarn Methode verglichen werden.
	
	\subsection{SURF}
	Auch SURF teilt das Fenster um den zu beschreibenden Merkmalspunkt in Abschnitte auf. Allerdings wird nun das Haar-Wavelet über jeden dieser Abschnitte berechnet und nicht deren Gradienten. Dies geschieht separat für x- und y-Richtung und einmal für Betrags- und einmal für Absolutwerte. Jeder Abschnitt summiert diese Werte gewichtet auf. Somit erhält man nur vier Werte pro Abschnitt.
	\begin{align}
		v = \begin{pmatrix}
		\sum \diff x & \sum \diff y & \sum \abs{\diff x} & \sum \abs{\diff y}
		\end{pmatrix}
	\end{align}
	
	Durch die halbierte Anzahl in der Deskriptor Information, lassen sich Merkmale viel schneller vergleichen und kleiner speichern. Außerdem ist die Berechnung selbst, durch den Einsatz der Haar-Wavelet Summen im Gegensatz zu den Gradienten, effizienter. Hinzu kommt, dass diese Methode weniger von Bildrauschen beeinflusst wird. \cite{Surf}

	\section{Zusammenfassung}
	In diesem Essay wurde die Vorgehensweise von SIFT und SURF, mit Fokus auf ihre Unterschiede, zusammengefasst. Dabei unterscheidet sich ihre Grundstruktur, genauso wie viele der Arbeitsweisen nicht. Zunächst werden Kandidaten durch eine Extremwertsuche bestimmt. Anschließend werden diese mit Blick auf Relevanz und Quantität gefiltert. Danach erfolgen Orientierungsnormierung und Deskription.
	
	Ein wichtiges Ergebnis des Vergleichs ist, dass SURF trotz des geringerem Berechnungsaufwands immer mindestens gleich gute Ergebnisse liefert. Diese wird vor allem zum einen durch die verwendete Approximation der Determinanten der Hesse-Matrix erreicht, zum anderen durch die Ersparnis bei der Erstellung und beim Matching der Deskriptoren der Merkmale. Zuletzt kann eine verbesserte Robustheit von SURF gegenüber Bildrauschen beobachtet werden. \cite{Surf}
	
	
	\bibliographystyle{natdin}
	\bibliography{literatur}

\end{document}