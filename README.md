# Computer Vision
## Zusammenfassung der Videovorlesungen
### Links

Homework: [repository](https://bitbucket.org/computervision/computer-vision)  
Download latest PDF: [PDF](https://lootsch.cetus.uberspace.de/latex/cvs.pdf)

## Written by
- Hofbauer, Markus
- Meyer, Kevin
