% Dokumenteinstellungen
% ======================================================================	

\documentclass[10pt,a4paper,parskip=half,DIV=12]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{scientific}
\usepackage[european]{circuitikz}
\usepackage[automark]{scrpage2}
\usepackage{titling}
\usepackage{bbm}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage[square,numbers]{natbib}

\pagestyle{scrheadings}
\clearscrheadfoot
\ihead[]{\thetitle}
\cfoot[]{\pagemark}
\addtokomafont{disposition}{\rmfamily}

\sisetup{locale=DE}

\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\rang}{rang}

% Dokumentbeginn
% ======================================================================

\begin{document}

\title{Essay Computer Vision}
\date{\today}
\author{Markus Hofbauer}
\maketitle

\begin{abstract}
\begin{center}
\textbf{\large Abstract}
\end{center}
Dieses Essay stellt einen Vergleich zwischen zwei populären Methoden zur Bestimmung von markanten und invarianten Merkmalen aus Bildern vor. Diese Merkmale sollen für Aufgaben wie Objekterkennung oder 3D-Rekonstruktion geeignet sein und dabei robust gegenüber äußeren Einflüssen sein. 

Bei den betrachteten Methoden handelt es sich um SIFT und SURF, die sich in ihren grundsätzlichen Struktur sehr ähnlich sind. Allerdings handelt es sich bei SURF um eine Weiterentwicklung zu SIFT und ist damit schneller trotz gleicher Ergebnisqualität.
\end{abstract}

\section{Einleitung}
In der Computer Vision spielen Merkmale (sogenannte Features) eine entscheidende Rolle. Features sind markante Bildpunkte, die unabhängig von Skalierung, Rotation, Translation, Perspektive und unempfindlich gegenüber Beleuchtungsvariationen, Kontrast und Rauschen sind. Diese Bildpunkte dienen zur Objekterkennung, Kamerakalibrierung, 3D-Rekonstruktion, etc..

Die im folgenden Abschnitten behandelten Artikel stellen jeweils einen Algorithmus zur Detektion bzw. Zuordnung von Features vor. Dabei arbeiten beide Algorithmen sehr ähnlich. Das lässt sich damit erklären, dass SURF \cite{Surf} (Speeded-up robust features) lediglich eine Abwandlung von SIFT \cite{Sift} (Scale-invariant feature transform) darstellt. Im Wesentlichen unterscheiden sich die Algorithmen durch ihre Filter. Während SIFT einen Difference of Gaussians (DoG) Detektor und einen eigenen Deskriptor verwendet, setzt SURF auf Mittelwertfilter, welche durch die Verwendung von Integralbildern mit konstantem Zeitaufwand berechnet werden können. Dabei wird ein auf Hesse-Matrix beruhender Detektor verwendet. SURF ist damit um ein vielfaches schneller als SIFT, trotz gleich guter Ergebnisqualität.

\section{SIFT}
\label{sec:sift}
SIFT ist ein Verfahren, das aus Bildern Merkmale unabhängig von ihrer Orientierung und Skalierung findet und beschreibt. Der von Lowe vorgeschlagene Algorithmus besteht aus vier Hauptschritten.
\begin{enumerate}
\item Suche von Extrema im skalierten Raum
\item Lokalisierung der Interessenpunkte
\item Berechnung der Orientierung
\item Beschreibung der Merkmalspunkte
\end{enumerate}

\subsection{Detektor}
Der Detektor findet die gesuchten Interessenpunkte an den Extremstellen im differentiellen Skalenraum. Der Skalenraum $L(x,y,\sigma)$ wird durch eine Faltung des Bildes $I(x,y)$ mit einem Gaußfilter $G(x,y,\sigma)$ erstellt \citep[Abschnitt 3]{Sift}:
\begin{align}
L(x,y,\sigma) = G(x,y,\sigma)*I(x,y) \label{eq:L}
\end{align}
mit
\begin{align}
G(x,y,\sigma) = \frac{1}{2\pi\sigma^2}\;e^{-\frac{(x^2+y^2)}{2\sigma^2}}
\end{align}
Für die Bestimmung der Interessenpunkte aus dem Skalenraum wird eine Funktion $D(x,y,\sigma)$ benötigt, die als Difference of Gaussians (DoG) bezeichnet wird. Die Berechnung von $D$ erfolgt über die Subtraktion benachbarter Skalierungen des Skalenraums:
\begin{align}
D(x,y,\sigma) = (G(x,y,k\sigma) - G(x,y,\sigma))*I(x,y) = L(x,y,k\sigma) - L(x,y,\sigma)
\end{align}

Zur Bestimmung von Extremwerten werden die Nachbarn jedes Pixel betrachtet. Um als Kandidat in Frage zu kommen, muss ein Pixel größer oder kleiner als alle seine 8 Nachbarn auf gleicher Ebene und als seine 9 Nachbarn auf der nächst höheren bzw. tieferen Ebene sein.
Die gefundenen Extrempunkte bilden die Kandidaten für potentielle Interessenpunkte \citep[Abschnitt 3.1]{Sift}.

\subsection{Deskriptor}
Ein Deskriptor ist ein Vektor, der die Umgebung eines gefundenen Featurepunktes eindeutig und invariant gegen Rotation und unterschiedliche Beleuchtung beschreibt. Damit soll es möglich sein das Feature in anderen Bildern wiederzufinden.

Der SIFT-Deskriptor berechnet zuerst für jeden detektierten Interessenpunkt eine Orientierung. Anschließend bestimmt man die Gradienten zu jedem Bildpunkt in der Umgebung des Features und gewichtet die Gradienten mittels einer kreisförmigen Gaußmaske. Von einem $4\times 4$ Teilbereich werden die Werte in einem Orientierungshistogramm addiert, das aus 8 möglichen Richtungen besteht. Jeder Deskriptor besteht aus 16 Teilbereichen. Damit besteht der Vektor aus 128 Elementen. Nun lässt sich bestimmen, ob es sich bei zwei Punkten aus verschiedenen Bildern um den gleichen Bildpunkt handelt. Dazu werden die beiden Punkte gesucht, die sich am ähnlichsten sind. Diese werden auch als nächste Nachbarn bezeichnet \citep[Abschnitt 6.1]{Sift}.

\section{SURF}
Bei SURF handelt es sich um eine Abwandlung des in \cite{Sift} vorgestellten Algorithmus. Die in \autoref{sec:sift} beschriebene Vorgehensweise bleibt erhalten. Der Inhalt der einzelnen Schritte unterscheidet sich allerdings.

\subsection{Detektor}
Die in SURF beschriebene Methode zur Detektierung der Merkmale benutzt eine einfache Approximation der Determinanten der Hesse-Matrix \citep[Abschnitt 3.2]{Surf}.
\begin{align}
	H(x, y, \sigma) = \begin{bmatrix} L_{xx}(x, y, \sigma) & L_{xy}(x, y, \sigma) \\  L_{yx}(x, y, \sigma) &  L_{yy}(x, y, \sigma)\end{bmatrix}
\end{align}
mit $L_{xx}(x, y, \sigma) = \frac{\partial^2}{\partial x^2}\, L(x,y,\sigma)$ (analog für $L_{xy}$ und $L_{yy}$). Vgl. \autoref{eq:L}. Die diskrete Approximation der Hesse-Matrix kann als Mittelwertfilter aufgefasst werden.\\
Dafür werden Integralbilder benutzt, die zur schnellen Berechnung von Pixelsummen in rechteckigen Bildausschnitten verwendet werden. Ein Integralbild beschreibt die Summe aller Pixel im Rechteck, das vom aktuellen Punkt und dem Ursprung aufgespannt wird \citep[Abschnitt 3.1]{Surf}.
\begin{align}
I_\Sigma(x,y) = \sum_{i=0}^{i<x}\sum_{j=0}^{j<y}I(x,y)
\end{align}
Damit lässt sich allgemein die Summe der Pixel in einem Rechteck beschreiben.
\begin{align}
I_\Sigma = I_\Sigma(x,y) + I_\Sigma(x+\Delta x,y+\Delta y) - (I_\Sigma(x+\Delta x,y) + I_\Sigma(x,y+\Delta y))
\label{eq:ISigma}
\end{align}
\autoref{eq:ISigma} beschreibt die diskrete Approximation der Determinante der Hesse-Matrix und wird zur Detektion der Featurepunkte verwendet.

Die Approximation der Hesse-Matirx mit Integralsummen wird als Mittelwertfilter bezeichnet. Damit unterscheidet sich der SURF-Detektor vom SIFT-Detektor, der einen Gaußfilter verwendet.

\subsection{Deskriptor}
Beim SURF-Deskriptor wird für die Rotationsinvarianz die dominante Orientierung mittel Haar-Wavelets berechnet und mit einer Gaußfunktion gewichtet \citep[Abschnitt 4.1]{Surf}. Wie bei SIFT wird um das Feature ein Quadrat gelegt und in $4\times 4$ Teilbereiche aufgeteilt. Von jedem Teilbereich wird das Haar-Wavelets berechnet und die Antworten horizontal ($\diff x$) und vertikal ($\diff y$) aufsummiert \citep[Abschnitt 4.2]{Surf}. Das gleiche geschieht für die Absolutwerte ($\abs{\diff x},\abs{\diff y}$):
\begin{align}
v = \begin{pmatrix}
\sum \diff x & \sum \diff y & \sum \abs{\diff x} & \sum \abs{\diff y}
\end{pmatrix}
\end{align}
Aus den Vektoren der einzelnen Teilbereiche setzten sich die Featurevektoren für jeden einzelnen Punkt mit einer Gesamtlänge von 64 zusammen. 

Anders als SIFT, der den Deskriptor eines Features über Gradienten bestimmt, besteht der SURF-Deskriptor aus den Integralsummen. Damit ist der Vektor des SURF-Deskriptors nur halb so lang wie der des SIFT-Deskriptors und einfacher zu berechnen. Zusätzlich zur Größeneinsparung ist der SURF-Deskriptor robuster gegenüber Bildrauschen \citep[Abschnitt 4.2, Abbildung 14]{Surf}.

\section{Zusammenfassung}
In den vorherigen Abschnitten wurden die Vorgehensweisen der SIFT und SURF Detektoren und Deskriptoren beschrieben. Darin zeigt sich die grundsätzlich ähnliche Arbeitsweise der Algorithmen. Bei beiden werden zunächst Kandidaten für Featurepunkte detektiert. Uninteressante Kandidaten werden herausgefiltert. Die verbleibenden Features werden über die Berechnung der Orientierung rotationsinvariant. Zuletzt wird für jeden der Featurepunkte ein Deskriptor berechnet. Mit diesem lassen sich Features untereinander vergleichen und gleiche Punkte in verschiedenen Bildern wiederfinden.

Die wichtigen Unterschiede der beiden Konzepte sind die Algorithmen der Detektoren und Deskriptoren. Beide liefern eine vergleichbare Ergebnisqualität, jedoch sind die Methoden des SURF-Algorithmus effizienter und weniger fehleranfällig. SURF setzt bei der Detektion auf eine Approximation der Determinanten Hesse-Matrix durch Integralbilder. Diese Methode ist schneller als die DoG-Methode des SIFT-Algorithmus. Bei der Beschreibung der Features setzt SURF auf Integralsummen, was einfacher und schneller zu berechnen ist als der Gradient, der in SIFT verwendet wird. Außerdem hat Bildrauschen einen geringeren Einfluss auf den SURF-Deskriptor. Damit stellt SURF einen, im Vergleich zu SIFT, verbesserten Algorithmus zur Objekterkennung dar.

\bibliographystyle{natdin}
\bibliography{literatur}


\end{document}